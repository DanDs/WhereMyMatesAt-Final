import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Place } from "../../models/place";
import { ExitPage } from "../exit/exit";
import { MyplacesServiceProvider } from "../../providers/myplaces-service/myplaces-service";
import { AngularFireAuth } from "angularfire2/auth";
import { User } from "firebase/app";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  myplaces: Place[];
  me: User;

  constructor(public navCtrl: NavController, public myplacesService: MyplacesServiceProvider, public alertCtrl: AlertController,  public afAuth: AngularFireAuth) {
    
    let suscriptor =myplacesService.getMyPlaces().subscribe(data => {
      this.myplaces = data;
    });
    this.me = this.afAuth.auth.currentUser;  
  }
  
  addPlace(){
    let prompt = this.alertCtrl.create({
      title: 'MyPlaces',
      message: "Adding a new place",
      inputs: [
        {
          name: 'Title',
          placeholder: 'Title'
        },       
        {
          name: 'latitude',
          placeholder: 'Latitude'
        },
        {
          name: 'longitude',
          placeholder: 'Longitude'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            var p;
            p = data;
            p.user = this.me.email
            this.myplacesService.newPlace(p);  
          }
        }
      ]
    });
    prompt.present();
  }

  exit(){
    this.navCtrl.push(ExitPage);
  }

  showOptions(item: Place){
    console.log(item.id);
    let alert = this.alertCtrl.create({
      title: 'Do you want to delete this location?',      
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: data => {            
            this.myplacesService.deletePlace(item.id); 
          }
        }
      ]
    });
    alert.present();
  }

}
