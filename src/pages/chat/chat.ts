import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Message } from "../../models/message";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import { Contact } from "../../models/contact";
import { AngularFireAuth } from "angularfire2/auth";
import { User } from "firebase/app";


/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  messages: Message[];
  msm: Message;
  @Input() contact: Contact;
  me: User;

  constructor(public navCtrl: NavController, public navParams: NavParams, public messageService: MessageServiceProvider, public afAuth: AngularFireAuth) {
    this.contact = navParams.get('contact');
    let suscriptor =messageService.getMessages().subscribe(data => {
      this.messages = data;
   });
   this.me = this.afAuth.auth.currentUser;      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
    console.log(this.me.email);   
    console.log(this.contact.email);   
  }

  sendMessage(mbody){    
    
    this.msm = new Message(this.me.email, this.contact.email, mbody);
    this.messageService.newMessage(this.msm);     
    console.log(this.me.email);   
    console.log(this.contact.email);   
  }

}
