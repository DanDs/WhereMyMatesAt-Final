import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeetingPointsPage } from './meeting-points';

@NgModule({
  declarations: [
    MeetingPointsPage,
  ],
  imports: [
    IonicPageModule.forChild(MeetingPointsPage),
  ],
})
export class MeetingPointsPageModule {}
