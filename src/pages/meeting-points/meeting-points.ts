import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { MeetingPointsServiceProvider } from "../../providers/meeting-points-service/meeting-points-service";
import { User } from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { MeetingPoint } from "../../models/meetingpoint";
import { ExitPage } from "../exit/exit";

/**
 * Generated class for the MeetingPointsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-meeting-points',
  templateUrl: 'meeting-points.html',
})
export class MeetingPointsPage {

  meetingpoints: MeetingPoint[];
  me: User;

  constructor(public navCtrl: NavController, public meetingpointsService: MeetingPointsServiceProvider, public alertCtrl: AlertController,  public afAuth: AngularFireAuth) {
    
    let suscriptor =meetingpointsService.getMeetingPoints().subscribe(data => {
      this.meetingpoints = data;
    });
    this.me = this.afAuth.auth.currentUser;  
  }
  
  addMeetingPoint(){
    let prompt = this.alertCtrl.create({
      title: 'Meeting Point',
      message: "Adding a new meeting point",
      inputs: [
        {
          name: 'Title',
          placeholder: 'Title'
        },       
        {
          name: 'description',
          placeholder: 'Description'
        },      
        {
          name: 'latitude',
          placeholder: 'Latitude'
        },
        {
          name: 'longitude',
          placeholder: 'Longitude'
        },
        {
          name: 'date',
          placeholder: 'Date (e. 12-03-2018)'
        },
        {
          name: 'user1',
          placeholder: 'Add user email 1'
        },
        {
          name: 'user2',
          placeholder: 'Add user email 2'
        },
        {
          name: 'user3',
          placeholder: 'Add user email 3'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            var p;
            p = data;
            p.creator = this.me.email
            this.meetingpointsService.addMeetingPoint(p);  
          }
        }
      ]
    });
    prompt.present();
  }

  exit(){
    this.navCtrl.push(ExitPage);
  }

  showOptions(item: MeetingPoint){
    if(item.creator == this.me.email){
      console.log(item.id);
      let alert = this.alertCtrl.create({
        title: 'Do you want to delete this meeting point?',      
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Delete',
            handler: data => {            
              this.meetingpointsService.deleteMeetingPoint(item.id); 
            }
          }
        ]
      });
      alert.present();
    }
  }

  showInfo(){          
      let alert = this.alertCtrl.create({
        title: 'This is a meeting point that has been shared with you',      
        buttons: [
          {
            text: 'Got it!',
            role: 'cancel',
            handler: () => {
              console.log('Got it! clicked');
            }
          }
        ]
      });
      alert.present();    
  }
}
