import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  GoogleMapsAnimation,
  MyLocation
 } from '@ionic-native/google-maps';
import { Component } from '@angular/core';
import { NavController, ToastController, AlertController } from 'ionic-angular';
import { Place } from "../../models/place";
import { ExitPage } from "../exit/exit";
import { MyplacesServiceProvider } from "../../providers/myplaces-service/myplaces-service";
import { AngularFireList } from "angularfire2/database";
import { Observable } from "rxjs/Observable";
import { User } from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  mapReady: boolean = false;
  map: GoogleMap;
  marker: Marker;
  markers: Place[];
  mkrs: Place[];
  place: Place;
  me: User;

  constructor(public navCtrl: NavController, private googleMaps: GoogleMaps,  public toastCtrl: ToastController, 
              public mymarkers: MyplacesServiceProvider, public alertCtrl: AlertController, public afAuth: AngularFireAuth) {
    let suscriptor = mymarkers.getMyPlaces().subscribe(data => {
      this.markers = data;
      var p1:Place;
      for (var key in this.markers) {               
        this.toPlace(JSON.stringify(this.markers[key]));
      }                
   });
   this.me = this.afAuth.auth.currentUser; 
  }

  toPlace(data: string) {
    let jsonData = JSON.parse(data);  

    this.place = new Place(jsonData.id, jsonData.Title, jsonData.latitude, jsonData.longitude, jsonData.user);
    console.log(this.place.title);
    console.log(this.place.user);

    this.map.addMarker({
      title: ''+this.place.title,
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: this.place.latitude,
        lng: this.place.longitude
      }
    })   
  }

  ionViewDidLoad() {       
    console.log("Showing markers");      
    this.loadMap();
  }

  loadMap() {     
  
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: -17.382198,
          lng: -66.159847
        },
        zoom: 18,
        tilt: 30
      }
    };

    this.map = this.googleMaps.create('map_canvas', mapOptions);

    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');
        this.mapReady = true;    
      });

     this.map.one(GoogleMapsEvent.MAP_CLICK).then(() => function(e){
      this.placeMarkerAndPanTo(e.latLng, this.map);
     });

  }

  placeMarkerAndPanTo(latLng, map) {
    this.map.addMarker({
      title: 'Here you are!',
      snippet: 'This is your current location.',
      position: latLng,
      animation: GoogleMapsAnimation.BOUNCE
    });   
    map.panTo(latLng);
  }
       
  onButtonClick() {
    
    if (!this.mapReady) {
      this.showToast('The map is not ready yet. Please try again.');
      return;
    }
    
    this.map.clear();
    var ml: MyLocation;
    this.map.getMyLocation()
      .then((location: MyLocation) => {
        console.log(JSON.stringify(location, null ,2));
        ml = location;        
        return this.map.animateCamera({
          target: location.latLng,
          zoom: 17,
          tilt: 30
        }).then(() => {
          
          return this.map.addMarker({
            title: 'Here you are!',
            snippet: 'This is your current location.',
            position: location.latLng,
            animation: GoogleMapsAnimation.BOUNCE
          });
        })
      }).then((marker: Marker) => {        
        marker.showInfoWindow();
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          this.showOptions(ml);
          //this.addPlace(ml);
        });
                  
        this.map.on(GoogleMapsEvent.MAP_LONG_CLICK)
        .subscribe((marker) => {
          this.map.addMarker({
            title: 'Pin',
            icon: 'green',
            animation: 'DROP',
            position: {
              lat: marker.getPosition().lat,
              lng: marker.getPosition().lng
            },
            draggable: true
          })
          console.log(marker);         
        });
      });
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });

    toast.present(toast);
  }

  showOptions(location: MyLocation){
    let prompt = this.alertCtrl.create({
      title: 'Want to save this place?',
      message: "",      
      buttons: [
        {
          text: 'No, thanks',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save this location',
          handler: data => {
            this.addPlace(location); 
          }
        }
      ]
    });
    prompt.present();
  }

  addPlace(location: MyLocation){
    let prompt = this.alertCtrl.create({
      title: 'Adding a new place',      
      inputs: [
        {
          name: 'Title',
          placeholder: 'Title'
        },       
        {
          name: 'latitude',
          placeholder: ''+location.latLng.lat
        },
        {
          name: 'longitude',
          placeholder: ''+location.latLng.lng
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            var p;
            p = data;
            p.user = this.me.email;
            p.latitude = location.latLng.lat;
            p.longitude = location.latLng.lng;
            this.mymarkers.newPlace(p);  
          }
        }
      ]
    });
    prompt.present();
  }

  exit(){
    this.navCtrl.push(ExitPage);
  } 
}
