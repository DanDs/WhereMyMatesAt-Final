export class MeetingPoint {
    
      id: String;
      public title: String;
      public description: String;
      public creator: String;
      public user1: String;
      public user2: String;
      public user3: String;
      public latitude: number;
      public longitude: number;
      public date: Date;      
    
      constructor(id: String, title: String, description: String, creator: String, user1: String, user2: String, user3: String, 
                latitude: number, longitude: number, date: Date) {
                    this.id = id;
                    this.title = title;
                    this.description = description;
                    this.creator = creator;
                    this.user1 = user1;
                    this.user2 = user2;
                    this.user3 = user3;
                    this.latitude = latitude;
                    this.longitude = longitude;
                    this.date = date;        
      }
      
    }