export class Message {
    
      from: String;
      to: String;
      body: String;      
    
      constructor(from: String, to: String, body: String) {
          this.from = from;
          this.to = to;
          this.body = body;
      }
      
    }