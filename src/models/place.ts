export class Place {
    
      id: String;
      public title: String;
      public user: String;
      public latitude: number;
      public longitude: number;
    
      constructor($key: String, title: String, latitude: number, longitude: number, user: String) {
        this.id = $key;
        this.title = title;
        this.latitude = latitude;
        this.longitude = longitude;
        this.user = user;
      }
      
    }