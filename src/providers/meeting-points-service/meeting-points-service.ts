import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from "rxjs/Observable";
import { MeetingPoint } from "../../models/meetingpoint";

/*
  Generated class for the MeetingPointsServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MeetingPointsServiceProvider {

  meetingpoints: AngularFireList<MeetingPoint>;
  mpShared: AngularFireList<MeetingPoint>;
  au: any;
  
  constructor(public http: HttpClient, 
    public contactsdb: AngularFireDatabase,
    public afAuth: AngularFireAuth ) {
    this.meetingpoints = contactsdb.list('/meetingpoints/');
    
    this.au = afAuth;
  }

  getMeetingPoints(): Observable<MeetingPoint[]>{    
    return this.meetingpoints.valueChanges();
  }

  getSharedMeetingPoint(){
    return this.mpShared.valueChanges();
  }

  addMeetingPoint(mp: MeetingPoint){
    var key = this.contactsdb.database.ref('/meetingpoints/').push(mp).key;    
    mp.id = key;
    var updates = {};
    updates['/meetingpoints/'+key] = mp;      
    this.contactsdb.database.ref().update(updates);
  }   

  deleteMeetingPoint(key: String): void {
    this.contactsdb.list('/meetingpoints/').remove(''+key);  
  }

  shareMeetingPoint(){

  }
  

}
