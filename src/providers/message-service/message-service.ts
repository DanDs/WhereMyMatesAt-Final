import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AngularFireList, AngularFireDatabase } from "angularfire2/database";
import { Observable } from "rxjs/Observable";
import { AngularFireAuth } from "angularfire2/auth";
import { Message } from "../../models/message";

/*
  Generated class for the MessageServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MessageServiceProvider {

  messages: AngularFireList<Message>;
    
  constructor(public http: HttpClient, 
    public messagesdb: AngularFireDatabase,
    public afAuth: AngularFireAuth ) {
    this.messages = messagesdb.list('/messages/');
  }

  getMessages(): Observable<Message[]>{    
    return this.messages.valueChanges();
  }

  newMessage(message: Message){
    this.messages.push(message);
  } 
}
