import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Place } from "../../models/place";
import { AngularFireList, AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from "rxjs/Observable";

/*
  Generated class for the MyplacesServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyplacesServiceProvider {

  myplaces: AngularFireList<Place>; 
  au: any;
  
  constructor(public http: HttpClient, 
    public myplacesdb: AngularFireDatabase,
    public afAuth: AngularFireAuth ) {
    this.myplaces = myplacesdb.list('/myplaces/'+afAuth.auth.currentUser.uid);
    this.au = afAuth;
    console.log(this.myplaces);
    var item: any;
    for (item in this.myplaces) {
     // console.log(item.$key);
    }
  }

  getMyPlaces(): Observable<Place[]>{    
    return this.myplaces.valueChanges();
  }

  newPlace(place: Place){    
    var key = this.myplacesdb.database.ref('/myplaces/'+this.au.auth.currentUser.uid).push(place).key;    
    place.id = key;
    var updates = {};
    updates['/myplaces/'+this.au.auth.currentUser.uid+'/'+key] = place;      
    this.myplacesdb.database.ref().update(updates);
  }

  deletePlace(key: String): void {
    this.myplacesdb.list('/myplaces/'+this.au.auth.currentUser.uid).remove(''+key);  
  }
}
