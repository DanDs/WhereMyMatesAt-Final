### :point_right: This starter repo has moved to the [ionic-team/starters](https://github.com/ionic-team/starters/tree/master/ionic-angular/official/tabs) repo! :point_left:



En el repositorio se encuentra un apk (sin firmar) de la aplicacion 
con el nombre de WhereMyMatesAt-Unsigned-Prototipe.apk 
La aplicacion fue testeada en un android Lollipop (v 5.0). Usar la misma version o superiores.

Comandos para su instalacion despues de clonar el repositorio y acceder a la carpeta

$npm install

Para hacer correr en el navegador usar el comando 

$ionic serve

Para hacer correr en un dispositivo android Lollipo (v. 5.0) o superiores
conectar el dispositivo a la computadora

$ionic cordova run android

Tambien se puede descargar el apk (sin firmar) al dispositivo e instalarlo.
